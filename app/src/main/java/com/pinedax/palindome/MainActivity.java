package com.pinedax.palindome;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnRun = (Button) findViewById(R.id.btnrun);
        final TextView tvResults = (TextView) findViewById(R.id.tvresult);
        final EditText etText = (EditText) findViewById(R.id.etInputText);


        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPalindrome = isPalindrome(etText.getText().toString());

                tvResults.setText(isPalindrome ? "True" : "False");
            }
        });
    }

    private boolean isPalindrome(String input) {

        if (input == null) {
            return false;
        }

        int inputLength = input.length();

        if (inputLength == 1) {
            return true;
        }

        int runTo = inputLength/2;

        for (int x=0; x < runTo; x++){
            int lastItem = inputLength-1-x;
            String first = input.substring(x, x+1);
            String last = input.substring(lastItem, lastItem+1);

            if (!first.equals(last)) {
                return false;
            }
        }

        return true;
    }
}
